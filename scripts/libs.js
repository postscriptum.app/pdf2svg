const path = require('path');
const fs = require('fs');
const fsp = fs.promises;

(async () => {
	const libsPath = path.resolve(__dirname, "../dist/libs");
	await fsp.mkdir(libsPath, { recursive: true });
	const pdfDistPath = require.resolve("pdfjs-dist");
	const pdfDistBasePath = path.resolve(pdfDistPath, "../..");
	await fsp.copyFile(path.resolve(pdfDistBasePath, "legacy/build/pdf.js"), path.resolve(libsPath, "pdf.js"));
	await fsp.copyFile(path.resolve(pdfDistBasePath, "legacy/build/pdf.worker.js"), path.resolve(libsPath, "pdf.worker.js"));

	const fontsPath = path.resolve(__dirname, "../dist/fonts");
	await fsp.mkdir(libsPath, { recursive: true });
	await fsp.cp(path.resolve(pdfDistBasePath, "standard_fonts"), fontsPath, { recursive: true });
})();
