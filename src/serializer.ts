import type {DOMElement, Attributes} from "./domstubs";
import type { Writable} from "stream";
import {Readable} from "stream";

interface SVGSerializerOptions {
	floatPrecision?: number | undefined,
	inheritedFontsAttr?: boolean;
}

export function serializeSvgToStream(elem: DOMElement, writable: Writable, options?: SVGSerializerOptions): Promise<unknown> {
	return new Promise((resolve, reject) => {
		const readable = Readable.from(svgSerializer(elem, options));
		readable.once('error', reject);
		writable.once('error', reject);
		writable.once('close', resolve);
		readable.pipe(writable);
	});
}

const attrToOptimize = ['dx', 'dy', 'x', 'y', 'd', 'points', 'font-size'];

function* svgSerializer(elem: DOMElement, options: SVGSerializerOptions = {}, inheritedAttrs?: Attributes): Generator<string> {
	const [prefix, localName] = elem.nodeName.split(':');
	const tagName = prefix == 'svg' ? localName : elem.nodeName;
	yield `<${tagName}`;
	if (elem.nodeName === "svg:svg") {
		yield ' xmlns="http://www.w3.org/2000/svg"';
		yield ' xmlns:xlink="http://www.w3.org/1999/xlink"';
	}

	let attrsToInherit: Attributes = {};
	if (options.inheritedFontsAttr && elem.nodeName == 'svg:text') {
		attrsToInherit = mostUsedFontsAttr(elem);
		for (const attrName in attrsToInherit) {
			elem.setAttribute(attrName, attrsToInherit[attrName]);
		}
	}

	for (const name in elem.attributes) {
		let val = elem.attributes[name];
		const isInherited = inheritedAttrs && name in inheritedAttrs && inheritedAttrs[name] === val;
		if (val && !isInherited) {
			if (options.floatPrecision !== undefined && attrToOptimize.includes(name) && typeof val == "string") val = floatPrecision(val, options.floatPrecision);
			yield ` ${name}="${xmlEncode(val)}"`;
		}
	}
	if (elem.childNodes.length || elem.textContent) {
		yield '>';
		if (elem.textContent) yield xmlEncode(elem.textContent);
		else if (elem.childNodes.length) {
			for (const child of elem.childNodes) yield* svgSerializer(child, options, attrsToInherit);
		}
		yield `</${tagName}>`;
	} else {
		yield '/>';
	}
}

const fontsAttrs = ['font-family', 'font-size'];

function mostUsedFontsAttr(elem: DOMElement): Attributes {
	const attrs: Attributes = {};
	for (const fontAttr of fontsAttrs) {
		const valsCount: { [key: string]: number } = {};
		for (const child of elem.childNodes) {
			if (child.hasAttribute(fontAttr)) {
				const val = child.attributes[fontAttr];
				if (!(val in valsCount)) valsCount[val] = 1;
				else valsCount[val] = valsCount[val] + 1;
			}
		}

		let mostUsedVal = undefined, mostUsedCount = -Infinity;
		for (const val in valsCount) {
			if (valsCount[val] > mostUsedCount) {
				mostUsedVal = val;
				mostUsedCount = valsCount[val];
			}
		}
		if (mostUsedVal !== null) {
			attrs[fontAttr] = mostUsedVal;
		}
	}
	return attrs;
}

function xmlEncode(s: string): string {
	let i = 0,
		ch;
	s = String(s);

	while (i < s.length && (ch = s[i]) !== "&" && ch !== "<" && ch !== '"' && ch !== "\n" && ch !== "\r" && ch !== "\t") {
		i++;
	}

	if (i >= s.length) {
		return s;
	}

	let buf = s.substring(0, i);

	while (i < s.length) {
		ch = s[i++];

		switch (ch) {
		case "&":
			buf += "&amp;";
			break;
		case "<":
			buf += "&lt;";
			break;
		case '"':
			buf += "&quot;";
			break;
		case "\n":
			buf += "&#xA;";
			break;
		case "\r":
			buf += "&#xD;";
			break;
		case "\t":
			buf += "&#x9;";
			break;
		default:
			buf += ch;
			break;
		}
	}

	return buf;
}


const reNumericValues = /^([-+]?\d*\.?\d+([eE][-+]?\d+)?)(px)?$/;
const reSeparator = /\s+,?\s*|,\s*/;

function floatPrecision(vals: string, precision: number): string {
	const roundedVals = [];
	for (const val of vals.split(reSeparator)) {
		const match = val.match(reNumericValues);
		if (match) {
			const num = Number(Number(match[1]).toFixed(precision));
			const unit = match[3] || '';
			const str = removeLeadingZero(num);
			roundedVals.push(str + unit);
		} else {
			roundedVals.push(val);
		}
	}
	return roundedVals.join(' ');
}

function removeLeadingZero(num: number): string {
	let strNum = num.toString();

	if (0 < num && num < 1 && strNum.charAt(0) === '0') {
		strNum = strNum.slice(1);
	} else if (-1 < num && num < 0 && strNum.charAt(1) === '0') {
		strNum = strNum.charAt(0) + strNum.slice(2);
	}
	return strNum;
}
