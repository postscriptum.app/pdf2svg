/**
 * This file is a derivative work of pdf.js
 *
 * Copyright 2021 Mozilla Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
"use strict";

export type Attributes = { [key: string]: string };

class DOMElement {
	nodeName: string;
	childNodes: DOMElement[];
	attributes: Attributes;
	textContent: string;
	sheet: {
		cssRules: any[];
		insertRule(rule: any): void;
	};

	constructor(name: string) {
		this.nodeName = name;
		this.childNodes = [];
		this.attributes = {};
		this.textContent = "";

		if (name === "style") {
			this.sheet = {
				cssRules: [],

				insertRule(rule) {
					this.cssRules.push(rule);
				}

			};
		}
	}

	hasAttribute(name: string): boolean {
		return name in this.attributes;
	}

	hasAttributeNS(ns: string, name: string): boolean {
		if (ns) {
			const suffix = ":" + name;
			for (const fullName in this.attributes) {
				if (fullName.slice(-suffix.length) === suffix) return true;
			}
		}
		return false;
	}

	getAttribute(name: string): string | null {
		if (this.hasAttribute(name)) return this.attributes[name];
		return null;
	}

	getAttributeNS(ns: string, name: string): string | null {
		if (this.hasAttribute(name)) return this.attributes[name];

		if (ns) {
			const suffix = ":" + name;

			for (const fullName in this.attributes) {
				if (fullName.slice(-suffix.length) === suffix) {
					return this.attributes[fullName];
				}
			}
		}

		return null;
	}

	setAttribute(name: string, value: string): void {
		this.attributes[name] = value || "";
	}

	setAttributeNS(ns: string, name: string, value: string): void {
		this.setAttribute(name, value);
	}

	appendChild(element: DOMElement): DOMElement {
		const childNodes = this.childNodes;

		if (!childNodes.includes(element)) {
			childNodes.push(element);
		}
		return element;
	}

	hasChildNodes(): boolean {
		return this.childNodes.length !== 0;
	}

	cloneNode(): DOMElement {
		const newNode = new DOMElement(this.nodeName);
		newNode.childNodes = this.childNodes;
		newNode.attributes = this.attributes;
		newNode.textContent = this.textContent;
		return newNode;
	}
}

const document = {
	childNodes: [] as DOMElement[],
	head: null as DOMElement,

	get currentScript() {
		return {
			src: ""
		};
	},

	get documentElement() {
		return this;
	},

	createElementNS(ns: string, tagName: string) {
		const elObject = new DOMElement(tagName);
		return elObject;
	},

	createElement(tagName: string) {
		return this.createElementNS("", tagName);
	},

	getElementsByTagName(tagName: string) {
		if (tagName === "head") {
			return [this.head || (this.head = new DOMElement("head"))];
		}

		return [];
	}

};

class Image {
	onload: () => void;

	constructor() {
		this._src = null;
		this.onload = null;
	}

	protected _src: string;

	get src(): string {
		return this._src;
	}

	set src(value) {
		this._src = value;

		if (this.onload) {
			this.onload();
		}
	}
}


function setStubs(namespace: { [key: string]: any }): void {
	namespace.document = document;
	namespace.Image = Image;
}

function unsetStubs(namespace: { [key: string]: any }): void {
	delete namespace.document;
	delete namespace.Image;
}

export {setStubs, unsetStubs, DOMElement};