#!/usr/bin/env node
import * as fs from "fs";
import {Command} from "commander";
import type {Writable} from "stream";
import * as path from "path";
import * as domstubs from "./domstubs.js";
import {serializeSvgToStream} from "./serializer.js";

const fsp = fs.promises;


function format(string: string, object: { [key: string]: string }): string {
	return string.replace(/\${(.*?)}/g, (...[, g]) => object[g]);
}

const pdfjs = require(path.resolve(__dirname, "..", "libs", "pdf.js"));
pdfjs.GlobalWorkerOptions.workerSrc = path.resolve(__dirname, "libs", "pdf.worker.js");
const standardFontDataUrl = path.resolve(__dirname, "..", "fonts") + path.sep;

domstubs.setStubs(global);

const SVG_NS = "http://www.w3.org/2000/svg";

function fileOutputError(file: string, e: NodeJS.ErrnoException): void {
	console.error(`Unable to output to the file '${file}'.`);
	if (e.code == 'EACCES') console.error('Permission denied');
	else if (e.code == 'EISDIR') console.error('The path corresponds to an existing directory.');
	else console.error(e);
	process.exit(1);
}

function pm(m: number[]): string {
	if (m[4] === 0 && m[5] === 0) {
		if (m[1] === 0 && m[2] === 0) {
			if (m[0] === 1 && m[3] === 1) {
				return "";
			}
			return `scale(${pf(m[0])} ${pf(m[3])})`;
		}
		if (m[0] === m[3] && m[1] === -m[2]) {
			const a = (Math.acos(m[0]) * 180) / Math.PI;
			return `rotate(${pf(a)})`;
		}
	} else {
		if (m[0] === 1 && m[1] === 0 && m[2] === 0 && m[3] === 1) {
			return `translate(${pf(m[4])} ${pf(m[5])})`;
		}
	}
	return (
		`matrix(${pf(m[0])} ${pf(m[1])} ${pf(m[2])} ${pf(m[3])} ${pf(m[4])} ` +
		`${pf(m[5])})`
	);
}

function pf(value: number): string {
	if (Number.isInteger(value)) {
		return value.toString();
	}
	const s = value.toFixed(10);
	let i = s.length - 1;
	if (s[i] !== "0") {
		return s;
	}

	// Remove trailing zeros.
	do {
		i--;
	} while (s[i] === "0");
	return s.substring(0, s[i] === "." ? i : i + 1);
}

const serialOptions = { floatPrecision: 3, inheritedFontsAttr: true};

(async () => {
	try {
		const command = new Command()
			.argument('<input>', 'PDF input file')
			.requiredOption('-o, --output <output>')
			.option("-p, --page <number...>", "Page selected by number to convert")
			.option("-f, --filename <format>", "Filename format to use when multiple SVG are outputted.", "${basename}-${page}.svg")
			.option("-c, --cmaps <path...>", "CMAP directory")
			.helpOption('-h, --help', 'Display help for command')
			.configureHelp({helpWidth: 80})
			.showHelpAfterError();

		command.parse();
		const args = command.processedArgs;
		const opts = command.opts();

		const input = args[0];
		let pdfData;
		if (input == "-") {
			// Not found how to use fsp.readFile with stdin, number argument not accepted
			pdfData = await fs.readFileSync(process.stdin.fd);
		} else if (fs.existsSync(input)) {
			pdfData = await fsp.readFile(input);
		} else {
			console.error(`Input file '${input}' not found.`);
			process.exit(1);
		}
		const pdf = await pdfjs.getDocument({
			data: new Uint8Array(pdfData),
			cMapPacked: true,
			verbosity: pdfjs.VerbosityLevel.WARNINGS,
			/* Required by the SVG backend */
			fontExtraProperties: true,
			standardFontDataUrl
		}).promise;

		let pageNumbers = opts.page;
		if (pageNumbers) pageNumbers = pageNumbers.map((page: string) => parseInt(page));
		else pageNumbers = Array.from({length: pdf.numPages}, (v, i) => i + 1);

		let outputStream: Writable;
		if (opts.output == '-') outputStream = process.stdout;
		else {
			if (opts.output.endsWith(path.sep)) {
				opts.output = path.resolve(opts.output);
				try {
					await fsp.mkdir(opts.output, {recursive: true});
				} catch (e) {
					console.error(`Unable to output to the directory '${opts.output}'.`);
					if (e.code == 'EEXIST') console.error('The path corresponds to an existing file.');
					else console.error(e);
					process.exit(1);
				}
			} else {
				try {
					opts.output = path.resolve(opts.output);
					outputStream = fs.createWriteStream(opts.output);
				} catch (e) {
					fileOutputError(opts.output, e);
				}
			}
		}

		const basename = path.basename(opts.output, '.pdf');

		let bottom = 0;
		let svgGfx: any, svg, views: { [ key: string]: Element };
		for (const pageNumber of pageNumbers) {
			try {
				const page = await pdf.getPage(pageNumber);
				const opList = await page.getOperatorList();
				const viewport = page.getViewport({scale: 96 / 72, offsetY: bottom});
				if (!svgGfx) {
					svgGfx = new pdfjs.SVGGraphics(page.commonObjs, page.objs, true);
					svgGfx.embedFonts = true;
					svg = svgGfx.svgFactory.create(viewport.width, viewport.height);
					const definitions = svgGfx.svgFactory.createElement("svg:defs");
					svg.appendChild(definitions);
					svgGfx.defs = definitions;
					views = {};
					const svgPageNumbers = outputStream ? pageNumbers : [ pageNumber ];
					for (const pageNumber of svgPageNumbers) {
						const view = document.createElementNS(SVG_NS, 'svg:view');
						view.setAttribute('id', 'page' + pageNumber);
						svg.appendChild(view);
						views[pageNumber] = view;
					}
				}

				svgGfx.viewport = viewport;
				const pageGroup = document.createElementNS(SVG_NS, 'svg:g');
				pageGroup.setAttributeNS(null, "transform", pm(viewport.transform));
				svg.appendChild(pageGroup);
				svgGfx.svg = pageGroup;
				svgGfx.objs = page.objs;
				await svgGfx.loadDependencies(opList);
				svgGfx.group(svgGfx.convertOpList(opList));

				views[pageNumber].setAttribute('viewBox', `0 ${bottom} ${viewport.width} ${viewport.height}`);
				bottom += viewport.height;

				if (!outputStream) {
					try {
						const pageFilename = format(opts.filename, {basename, page: pageNumber.toString()});
						const pageOutputStream = fs.createWriteStream(path.resolve(opts.output, pageFilename));
						await serializeSvgToStream(svg, pageOutputStream, serialOptions);
					} catch (e) {
						fileOutputError(opts.output, e);
					} finally {
						svgGfx = null;
						bottom = 0;
					}
				}
			} catch (e) {
				console.error(`An error occured on page ${pageNumber}`);
				console.error(e);
			}
		}
		if (outputStream) await serializeSvgToStream(svg, outputStream, serialOptions);
	} catch (e) {
		console.error(e);
	}
})();
